// Themes begin
// am4core.useTheme(am4themes_frozen);
am4core.useTheme(am4themes_animated);
// Themes end
function recreateChart(chartData) {
  // Delete old chart (if esistent)
  document.getElementById('chartdiv').innerHTML = "";
  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.PieChart);
  // Add data
  chart.data = chartData;
  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "amount";
  pieSeries.dataFields.category = "expense";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.slices.template.strokeWidth = 2;
  pieSeries.slices.template.strokeOpacity = 1;
  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;

}