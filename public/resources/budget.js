function getExpense() {
  fetch('http://localhost:8080/expense/0/0', {
      headers: addAuthorizationHeader({})
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(stats => {
      displayExpense(stats.expenses);
      displayBudgetValue(stats.monthBudget, stats.currentSpending);
    });
}

function displayExpense(expenses) {
  let expenseList = document.getElementById('expenseList');
  expenseList.innerHTML = "";

  for (let i = 0; i < expenses.length; i++) {
    let expenseRow = " ";
    expenseRow = "<tr>";
    expenseRow = expenseRow + "<td>" + expenses[i].id;
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + "<td>" + expenses[i].date;
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + "<td>" + expenses[i].value;
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + "<td>" + expenses[i].description;
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + "<td>" + expenses[i].category;
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + '<td>';
    expenseRow = expenseRow + '<div class ="expenseRowButtons"><button type="button" id="kustutaNupp" class="btn btn-danger btnDelete" onClick = "deleteExpense(' + expenses[i].id + ')" >Kustuta</button></div>' +
      '<div class="expenseRowButtons"><button type="button" id="muudaNupp" class="btn btn-info btnEdit" onClick="openExpenseModal(' + expenses[i].id + ')">Muuda</button></div>';
    expenseRow = expenseRow + "</td>";
    expenseRow = expenseRow + "</tr>";
    expenseList.innerHTML += expenseRow;
  }

  if (expenses.length > 0) {
    let chartData = createChartData(expenses);
    recreateChart(chartData);
  } else {
    let chartData = document.getElementById('chartdiv');
    chartData.innerHTML = "";
  }
}

function createChartData(expenses) {
  let categoryDict = {};
  for (let i = 0; i < expenses.length; i++) {
    if (categoryDict[expenses[i].category] == null) { // kirjet veel ei ole
      categoryDict[expenses[i].category] = expenses[i].value;
    } else { // see kategooriakirje on juba olemas
      categoryDict[expenses[i].category] += expenses[i].value;
    }
  }
  let chartData = [];
  const availableCategories = Object.keys(categoryDict);
  for (let i = 0; i < availableCategories.length; i++) {
    chartData.push({
      "expense": availableCategories[i],
      "amount": categoryDict[availableCategories[i]],
    });
  }

  return chartData;
}

function displayBudgetValue(budgetValue, currentTotal) {
  let monthBudgetValueDiv = document.getElementById('monthBudgetValueDiv');
  let monthBudgetValue = document.getElementById('monthBudgetValue');

  if (budgetValue <= 0) {
    monthBudgetValueDiv.style.visibility = 'hidden';
    balanceDiv.style.visibility = 'hidden';
  } else {
    monthBudgetValueDiv.style.visibility = 'visible';
    balanceDiv.style.visibility = 'visible';
  }

  monthBudgetValue.innerHTML = budgetValue;
  let currentSpending = document.getElementById('currentSpending');
  currentSpending.innerHTML = currentTotal;
  let balance = document.getElementById('balance');
  balance.innerHTML = budgetValue - currentTotal;
}

function displayExpenseByMonth() {
  const year = document.getElementById('year').value;
  const month = document.getElementById('month').value;
  const searchUrl = 'http://localhost:8080/expense/' + year + "/" + month;
  fetch(searchUrl, {
      headers: addAuthorizationHeader({})
    })
    .then(response => response.json())
    .then(stats => {
      displayExpense(stats.expenses);
      displayBudgetValue(stats.monthBudget, stats.currentSpending);
      closeFilterModal()
    });
}

function closeFilterModal() {
  $('#expenseFilterModal').modal('hide');
}

function addExpense() {

  const date = document.getElementById('expenseDate').value;
  const value = document.getElementById('expenseValue').value;
  const description = document.getElementById('expenseDescription').value;
  const category = document.getElementById('expenseCategory').value;
  const editUrl = 'http://localhost:8080/expense/';
  fetch(editUrl, {
      method: 'POST',
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'date': date,
        'value': value,
        'description': description,
        'category': category
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      displayExpenseByMonth();
      closeExpenseModal();
    });
}

function addBudget() {
  const year = document.getElementById('budgetYear').value;
  const month = document.getElementById('budgetMonth').value;
  const monthBudget = document.getElementById('budgetValue').value;
  const editUrl = 'http://localhost:8080/budget';
  fetch(editUrl, {
      method: 'POST',
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'year': year,
        'month': month,
        'monthBudget': monthBudget
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      document.getElementById('year').value = year;
      document.getElementById('month').value = month;
      displayExpenseByMonth();
      closeBudgetModal();
    });
}

function closeBudgetModal() {
  $('#budgetModal').modal('hide');

}


function editExpense() {
  const id = document.getElementById('expenseId').value;
  const date = document.getElementById('expenseDate').value;
  const value = document.getElementById('expenseValue').value;
  const description = document.getElementById('expenseDescription').value;
  const category = document.getElementById('expenseCategory').value;
  const editUrl = 'http://localhost:8080/expense/';
  fetch(editUrl, {
      method: 'PUT',
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'id': id,
        'date': date,
        'value': value,
        'description': description,
        'category': category
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      displayExpenseByMonth();
      closeExpenseModal();
    });
}

function deleteExpense(id) {
  if (confirm("Kas olete kindel, et soovite antud kulurida kustutada?")) {

    fetch("http://localhost:8080/expense/" + id, {
        method: 'DELETE',
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => displayExpenseByMonth());
  }
}

function openExpenseModal(id) {
  $('#exampleModal').modal('show');
  document.getElementById('expenseId').value = null;
  document.getElementById('expenseDate').value = null;
  document.getElementById('expenseCategory').value = "";
  document.getElementById('expenseValue').value = null;
  document.getElementById('expenseDescription').value = null;


  if (id > 0) {
    const getExpenseUrl = "http://localhost:8080/expense/" + id;
    fetch(getExpenseUrl, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))

      .then(response => response.json())
      .then(expense => {
        document.getElementById('expenseId').value = expense.id;
        document.getElementById('expenseDate').value = expense.date;
        document.getElementById('expenseValue').value = expense.value;
        document.getElementById('expenseDescription').value = expense.description;
        document.getElementById('expenseCategory').value = expense.category != null ? expense.category : "";
      });
  }
}

function addOrEditExpense() {
  if (validateExpense()) {
    const id = document.getElementById('expenseId').value;
    if (id > 0) {
      editExpense();
    } else {
      addExpense();
    }
  }
}

function validateExpense() {
  const date = document.getElementById('expenseDate').value;
  const category = document.getElementById('expenseCategory').value;
  const value = document.getElementById('expenseValue').value;
  const description = document.getElementById('expenseDescription').value;


  if (date == null || date.length < 1) {
    displayExpenseValidationError("Kuupäev ei ole valitud");
    return false;
  }
  if (category == 0) {
    displayExpenseValidationError("Kulutüüp ei ole valitud");
    return false;
  }
  if (value == null || value.length < 1) {
    displayExpenseValidationError("Summa ei ole sisestatud");
    return false;
  }
  if (description == null || description.length < 1) {
    displayExpenseValidationError("Kommentaar ei ole sisestatud");
    return false;
  }

  hideExpenseValidationError();
  return true;
}

function closeExpenseModal() {
  $('#exampleModal').modal('hide');

}

function displayExpenseValidationError(errorText) {
  let errorDiv = document.getElementById('expenseValidationError');
  errorDiv.style.display = 'block';
  errorDiv.innerHTML = errorText;
}

function hideExpenseValidationError() {
  let errorDiv = document.getElementById('expenseValidationError');
  errorDiv.style.display = 'none';
  errorDiv.innerHTML = '';
}

$(function() {
  $("#expenseDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

function drawStuff(menu) {
  if (menu.value == '1') {
    document.getElementById('expenseCategory').innerHTML = "toidukulud";

  } else if (menu.value == '2') {
    document.getElementById('expenseCategory').innerHTML = "transpordi kulud";

  } else if (menu.value == '3') {
    document.getElementById('expenseCategory').innerHTML = "eluasemekulud";
  } else if (menu.value == '4') {
    document.getElementById('expenseCategory').innerHTML = "muud kulud";
  }

}

function openExpenseModal1(id) {
  document.getElementById('expenseId').value = null;
  document.getElementById('expenseValue').value = null;
  document.getElementById('expenseDescription').value = null;
  $('#exampleModal1').modal('show');

  if (id > 0) {
    fetch('http://localhost:8080/expense/' + id)
      .then(response => response.json())
      .then(expense => {
        document.getElementById('expenseId').value = expense.id;
        document.getElementById('expenseDate').value = expense.date;
        document.getElementById('expenseValue').value = expense.value;
        document.getElementById('expenseDescription').value = expense.description;

      });
  }
}

function myFunction() {
  const style = document.getElementById("kuludeTabel").classList.toggle("expenses-list-hidden")
  style.visibility = "collapse"
}
//kaunistamine